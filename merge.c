#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

/* Prototipos */

void bloq();
void desbloq();

/* Declaraciones */

int npp_max;
int *shm_deg;
int npp_max;
int sem_id;
int shm_id1;
int shm_id2;
int *arrpid;

void bloq()
{
	struct sembuf opts[1];

	opts[0].sem_num = 0;
	opts[0].sem_op = -1;
	opts[0].sem_flg = 0;

	if (semop(sem_id, opts, 1) == -1) {
		/* Detach from the shared memory now that we are done using it. */
		shmdt(shm_deg);
		/* Delete the shared memory segment. */
		shmctl(shm_id1, IPC_RMID, NULL);
		shmctl(shm_id2, IPC_RMID, NULL);
		semctl(sem_id, 0, IPC_RMID);
		exit(1);
	}
}

void desbloq()
{
	struct sembuf opts[1];

	opts[0].sem_num = 0;
	opts[0].sem_op = 1;
	opts[0].sem_flg = 0;

	if (semop(sem_id, opts, 1) == -1) {
		/* Detach from the shared memory now that we are done using it. */
		shmdt(shm_deg);
		/* Delete the shared memory segment. */
		shmctl(shm_id1, IPC_RMID, NULL);
		shmctl(shm_id2, IPC_RMID, NULL);
		semctl(sem_id, 0, IPC_RMID);
		exit(1);
	}
}

void load() 
{
	FILE *foo = fopen("puds.txt", "r");
	int k=0;
	int data;
	while (fscanf(foo, "%d", &data) != EOF) {
		++k;
		arrpid = (int *) realloc(arrpid, k * sizeof(int));
		arrpid[k - 1] = data;
	}
}

void display(int array[], int length)
{
	int i;
	printf(">");
	for (i = 0; i < length; i++)
		printf(" %d", array[i]);
	printf("\n");
}

void display2(int *array, int length)
{
	int i;
	printf(">");
	for (i = 0; i < length; i++)
		printf(" %d", array[i]);
	printf("\n");
}

void merge(int *left, int llength, int *right, int rlength)
{
	/* Ubicaciones de memoria temporales para meclar los 2 segmentos del ayyay. */	
	int *ltmp = (int *) malloc(llength * sizeof(int));
	int *rtmp = (int *) malloc(rlength * sizeof(int));

	/* Punteros a los elementos que se ordenan en las ubicaciones de memoria temporal. */
	int *ll = ltmp;
	int *rr = rtmp;

	int *result = left;

	/* Copia el segmento del array que se mezclará en memoria temporal.*/
	memcpy(ltmp, left, llength * sizeof(int));
	memcpy(rtmp, right, rlength * sizeof(int));
		
	if ( (*shm_deg < npp_max) ) {
		for (int i = 0; i < npp_max; i++)
		{
			if(arrpid[i]==getpid())
			{							
				printf("Proceso %d pid %d", i,  getpid());
				printf("\n");
				printf("Izq ");
				display2(ltmp, llength);
				printf("Der ");
				display2(rtmp, rlength);
				++*shm_deg;
			}
		}
	}
		
	while (llength > 0 && rlength > 0) {
		if (*ll <= *rr) {
			/* Combina el primer elemento de la izquierda con el array principal
			   si es más pequeño o igual que el de la derecha. */
			*result = *ll;
			++ll;
			--llength;
		} else {
			/* Combina el primer elemento de la derecha con el array principal
			   si es más pequeño o igual que el de la izquierda. */
			*result = *rr;
			++rr;
			--rlength;
		}
		++result;	
	}
	/* Todos los elementos del segmento de array temporal ya sean,
	   izquierda o derecha se han mezclado de nuevo en el array principal.
	   Se agregan los elementos restantes del otro array temporal nuevamente al principal. */
	if (llength > 0)
		while (llength > 0) {
			/* Agregando el resto del array temporal izquierdo. */
			*result = *ll;
			++result;
			++ll;
			--llength;
		}
	else
		while (rlength > 0) {
			/* Agregando el resto del array temporal derecho */
			*result = *rr;
			++result;
			++rr;
			--rlength;
		}

	free(ltmp);
	free(rtmp);
}

void mergesort(int array[], int length)
{
	/* Indice medio del array y la longitud del array derecho */	
	int mid;

	int *left, *right;
	int llength;

	int lchild = -1;
	int rchild = -1;

	int status;

	FILE *foo = fopen("puds.txt", "aw");

	if (length <= 1)
		return;

	/* Division para truncamiento */
	mid = length / 2;

	llength = length - mid;

	 /* Asignacion de punteros a los segmentos del array a ser mezclado */

	left = array;
	right = array + llength;

	bloq();
	if (*shm_deg < npp_max) {
		/* Bajo el limite, fork */
		printf("Proceso %d con pid %d", *shm_deg, getpid());		
		display2(left, llength);
		++*shm_deg;
		desbloq();

		lchild = fork();
		if (lchild < 0) {
			perror("fork");
			exit(1);
		}
		if (lchild == 0) {
			mergesort(left, llength);
 			bloq();
			--*shm_deg;
			desbloq();
			exit(0);
		}
	} else {
		desbloq();
		mergesort(left, llength);
	}

	bloq();
	if (*shm_deg < npp_max) {
		/* Bajo el limite, fork */
		printf("Proceso %d con pid %d", *shm_deg, getpid());
		display2(right, mid);	
		++*shm_deg;
		desbloq();

		rchild = fork();
		if (rchild < 0) {
			perror("fork");
			exit(1);
		}
		if (rchild == 0) {
			mergesort(right, mid);
			exit(0);
		}
	} else {
		desbloq();
		mergesort(right, mid);
	}

	fprintf(foo, "%d\n", getpid());
	fclose(foo);
	waitpid(lchild, &status, 0);
	waitpid(rchild, &status, 0);
	load();
	merge(left, llength, right, mid);
}

int main(int argc, char *argv[])
{
	int *array = NULL;
	int length = 0;
	FILE *fh;
	int data;

	int *shm_array;
	int shm_id;
	key_t key;
	int i;
	size_t shm_size;

	union semun {
		int val;
		struct semid_ds *buf;
		ushort *array;
	} argument;

	if (argc != 3) {
		printf("Uso: %s <dop> <file>\n", argv[0]);
		return 1;
	}

	npp_max = atoi(argv[1]);

	arrpid = (int *) realloc(arrpid, npp_max * sizeof(int));

	printf("Maximo nro de procesos paralelos: %d\n", npp_max);

	/* Inicializacion de datos. */	
	printf("Archivo a ordenar: %s\n", argv[2]);

	fh = fopen(argv[2], "r");
	if (fh == NULL) {
		printf("archivo no encontrado\n");
		return 0;
	}

	while (fscanf(fh, "%d,", &data) != EOF) {
		++length;
		array = (int *) realloc(array, length * sizeof(int));
		array[length - 1] = data;
	}
	fclose(fh);
	remove("puds.txt");
	printf("%d Elementos leidos\n", length);

	/* Usar el pid de este proceso como la clave identificadora de la memoria compartida */	
	key = IPC_PRIVATE;

	/* Create the shared memory segment. */
	shm_size = length * sizeof(int);
	if ((shm_id = shmget(key, shm_size, IPC_CREAT | 0666)) == -1) {
		perror("shmget");
		exit(1);
	}

	shm_size = sizeof(int);
	if ((shm_id2 = shmget(key, shm_size, IPC_CREAT | 0666)) == -1) {
		fprintf(stderr, "[%d] ", getpid());
		perror("shmget");
		exit(1);
	}

	sem_id = semget(IPC_PRIVATE, 1, IPC_CREAT | 0666);

	argument.val = 1;
	if (semctl(sem_id, 0, SETVAL, argument) == -1) {
		fprintf(stderr, "[%d] ", getpid());
		perror("semctl");
		exit(1);
	}

	/* Attached to the shared memory segment in order to use it. */
	if ((shm_array = shmat(shm_id, NULL, 0)) == (int *) -1) {
		perror("shmat");
		exit(1);
	}

	if ((shm_deg = shmat(shm_id2, NULL, 0)) == (int *) -1) {
		fprintf(stderr, "[%d] ", getpid());
		perror("shmat");
		exit(1);
	}
	*shm_deg = 1;

	/*
	 * Copia de datos a ser ordenados de la memoria local a la compartida
	 */
	for (i = 0; i < length; i++) {
		shm_array[i] = array[i];
	}

	printf("Proceso 0 con pid %d", getpid());	
	display(shm_array, length);
	mergesort(shm_array, length);
	printf("\nArray ordenado\n");
	display(shm_array, length);

	return 0;
}