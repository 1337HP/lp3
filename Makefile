all: merge

merge: merge.c
	gcc -g -Wall $< -o $@

clean:
	rm -f merge
